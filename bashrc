#!/bin/bash


#-------------------------------------------------------------------------------
# Stijn Symons dotfiles
#-------------------------------------------------------------------------------

export DEFAULT_USERNAME='stijn'

# Basics
: ${HOME=~}
: ${LOGNAME=$(id -un)}
: ${UNAME=$(uname)}

# Fucking mail notifications
unset MAILCHECK

# Proper locale
: ${LANG:="en_US.UTF-8"}
: ${LANGUAGE:="en"}
: ${LC_CTYPE:="en_US.UTF-8"}
: ${LC_ALL:="en_US.UTF-8"}
export LANG LANGUAGE LC_CTYPE LC_ALL

# editor
export EDITOR=vim

#-------------------------------------------------------------------------------
# Path
#-------------------------------------------------------------------------------
export PATH=/usr/local/bin:~/.composer/vendor/bin/:$PATH

#-------------------------------------------------------------------------------
# Aliases
#-------------------------------------------------------------------------------

# shell
LS_OPTIONS=""
if [ "$UNAME" == "Linux" ]; then
    LS_OPTIONS="--color"
fi
alias l='ls -lAhF $LS_OPTIONS'
alias ll='ls -lAhF $LS_OPTIONS'
alias dir='ls -ld $LS_OPTIONS'

# git
alias g='git add . && git commit && git push'
alias gs='git status'
alias gd='git diff'
alias gb='git branch'
alias ga='git add'
alias gc='git commit'
alias gp='git push'

# vagrant
alias v='vagrant'
alias vs='cd ~/mandrill; vagrant ssh; cd -'
alias vu='cd ~/mandrill; vagrant up; cd -'
alias vh='cd ~/mandrill; vagrant halt; cd -'

# navigation
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias dev='cd ~/mandrill/srv'

# editor aliases
if [ "$UNAME" == "Darwin" ]; then
    if [ ! -f "/usr/local/bin/subl" ]; then 
    	if [ -f /Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl ]; then
            echo "Linking "
    		sudo ln -s "/Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl" /usr/local/bin/subl
    	fi
    fi
    alias s='subl'
fi

# misc
alias password='php -r "\$password=PHP_EOL;for(\$length=0;\$length<12;\$length++){\$password.=chr(rand(40,122));}echo \$password.PHP_EOL.PHP_EOL;"'
alias tprofile="curl -w '\nLookup time:\t%{time_namelookup}\nConnect time:\t%{time_connect}\nPreXfer time:\t%{time_pretransfer}\nStartXfer time:\t%{time_starttransfer}\n\nTotal time:\t%{time_total}\n' -o /dev/null -s "

#-------------------------------------------------------------------------------
# AMAZON
#-------------------------------------------------------------------------------
#if [ -d $HOME/.aws ]; then
#export JAVA_HOME="$(/usr/libexec/java_home)"
#export EC2_PRIVATE_KEY="$(/bin/ls "$HOME"/.ec2/pk-*.pem | /usr/bin/head -1)"
#export EC2_CERT="$(/bin/ls "$HOME"/.ec2/cert-*.pem | /usr/bin/head -1)"
#export EC2_HOME="/usr/local/Cellar/ec2-api-tools/1.6.12.0/libexec"
#
export AWS_RDS_HOME="/usr/local/Cellar/rds-command-line-tools/1.3.003/jars"
export EC2_REGION="eu-west-1"
export AWS_DEFAULT_REGION="eu-west-1"
#fi

if [ -d $HOME/.aws ]; then
    for KEYFILE in $HOME/.aws/*.keys; do
        source $KEYFILE
    done
fi

#-------------------------------------------------------------------------------
# Autocompleters
#-------------------------------------------------------------------------------
for name in $HOME/dotfiles/autocomplete/*; do
	source $name
done

# brew autocompleters (if any)
# install "brew install bash-completion" for this
if [ `which brew` ]; then
	if [ -f $(brew --prefix)/etc/bash_completion ]; then
		. $(brew --prefix)/etc/bash_completion
	else
		echo "Dude, you should install"
		echo "  brew install bash-completion"
	fi
else
	if [ $(uname) == "Darwin" ]; then
		echo "Dude, you should install brew on a mac, check it out here:"
		echo "  http://mxcl.github.com/homebrew/"
	fi
fi

complete -C aws_completer aws

#-------------------------------------------------------------------------------
# Prompt
#-------------------------------------------------------------------------------
export LSCOLORS="gxcxfxdxbxegedabagacad"
export CLICOLOR=1

YELLOW="\[\e[33;1m\]"
GREEN="\[\e[32;1m\]"
BLUE="\[\e[34;1m\]"
GREY="\[\e[0m\]"
LIGHT_CYAN="\[\033[1;36m\]"
DARK_GREY="\[\e[38;05;241m\]"

function parse_git_dirty() {
     git diff --quiet --ignore-submodules HEAD 2>/dev/null; [ $? -eq 1 ] && echo '*'
}

function parse_git_branch() {
     git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e "s/* \(.*\)/ \1$(parse_git_dirty)/"
}

function usernamehost() {
     if   [ $USER == $DEFAULT_USERNAME ]; then echo "";
     elif [ $USER == "vagrant" ]; then echo "${LIGHT_CYAN}\u ";
     else echo "${YELLOW}\u@\h ";
     fi
}

export PS1="$(usernamehost)${GREEN}\w${DARK_GREY}\$(parse_git_branch) ${GREEN}→ ${GREY}"
force_color_prompt=yes
